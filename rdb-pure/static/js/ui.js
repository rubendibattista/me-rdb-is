
$(document).ready(function() {

    if ($('#scene'.length > 0)){
        $('#scene').parallax();
    }
    
    var isMobile = /Mobi/.test(navigator.userAgent)
    if (isMobile) {
        $('.yt-embed').find('iframe').each(function(i, iframe){
            replaceIframe(iframe);
            
        });
    }

    if ($('.cssProgress').length > 0){
    
        var isElementInView = isScrolledIntoView($('.cssProgress'));
        if (!isElementInView) {
        $('.cssProgress').progress_fnc({ type: 'reset' });
        }
        else {
            $('.cssProgress').progress_fnc();
        }

        // Responsive progress bar
        $(window).scroll(function(){
            var isElementInView = isScrolledIntoView($('.cssProgress'));

            if (isElementInView) {
                $('.cssProgress').progress_fnc();
            }
        });
    }

});
    
    
function isScrolledIntoView(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $('#sidebar').height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((docViewTop <= elemTop) && (docViewBottom > elemBottom));
}

function replaceIframe(iframe)
{   
    var re = /embed\/(.*)/
    var src = $(iframe).attr('src').replace(re, "watch?v=$1");
    //var src = $(iframe).attr('src').replace("/embed/(.*)/", "/watch?v=$1");
    var html = '<a href="'+src+'"><i class="fa fa-3x fa-youtube"></i></a>';
    var parentDiv = $(iframe).parent('.yt-embed');
    parentDiv.html(html);
}

