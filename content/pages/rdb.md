Title: Ruben Di Battista
Subtitle: Foolish Geek <i title="Rocket" class="fa fa-rocket fa-2x"></i> Scientist Wannabe
Template: personal-page
Slug: me
Telegram: https://t.me/rdbisme
Quora: https://www.quora.com/profile/Ruben-Di-Battista
LinkedIN: https://it.linkedin.com/in/rubendibattista
Twitter: https://twitter.com/rdbisme
StackExchange: http://stackexchange.com/users/1407373/solidsnake?tab=accounts
Github: https://github.com/rdbisme
Gitlab: https://gitlab.com/rdbisme
email: rubendibattista@gmail.com

## How my quest started...
### <span style="color:rgb(255,122,164);"> <i class="fa fa-2x icon-atom"></i> Science Druid</span>
Hello, my name is Ruben Di Battista, a passionate rocket scientist wannabe currently resident in France. I always loved math and science since High School, while I was not busy playing <i title="videogames" class="fa fa-gamepad"></i>... In facts videogames are somehow the reason why I chose to start this amazing <i title="journey" class="fa fa-suitcase"></i> (thanks [Mr. Hideo Kojima](https://en.wikipedia.org/wiki/Hideo_Kojima)), I believed that for making them I had to become an Engineer (maybe that's a still valid assumption)! Later on, maybe since I was playing too much Ace Combat <i class="fa fa-fighter-jet"></i>, I had this idea of choosing Aerospace Engineering and I decided to pick the best place in Italy to obtain that <i title="achievement" class="fa fa-trophy"></i>: **Politecnico Di Milano**.

## A hidden side quest
### <span style="color:rgb(51,187,187);">+ <i class="fa fa-magic"></i> Rocket Science Passion</span><br /><span style="color:rgb(255,187,102);"><i class="fa fa-user-plus"></i> Team Working (+3)</span>, <span style="color:rgb(255,187,102);"><i class="fa fa-user-plus"></i> Leadership (+4)</span>, <span style="color:rgb(255,187,102);"><i class="fa fa-user-plus"></i> Project Management (+4)</span>, <span style="color:rgb(255,187,102);"><i class="fa fa-user-plus"></i> Enthusiasm (+4)</span>

As in all the best RPGs, while randomly and somehow unconsciously lurking around to find your way to the final boss and dropping the best equipment, serendipitously you encounter a random NPC, that proposes you a side adventure with promises of richness and glory (well, in my case the loot was just a lot of fun making <i title="rockets" class="fa fa-rocket"></i>) and with the possibility of joining the Guild. That's how I entered [Skyward Experimental Rocketry](http://www.skywarder.eu/blog/en/team-14-15/) and obtained the sacred spell of **Rocket Science Passion**. I trained and put a lot of effort in the duties for the Guild and I managed to become *Gran Maester of the Guild*... I remember the day of celebration, a snow storm was raging, and a [mystic creature made of fire, sugar, KNO3, HTPB and fiberglass was traveling the sky...](http://www.skywarder.eu/blog/en/rocksanne-i-x/)

<div class="yt-embed">
    <iframe width="100%" height="480" src="https://www.youtube.com/embed/7nLjASc7Ad8" frameborder="0" allowfullscreen></iframe>
</div>

[More ...]({filename}/pages/skyward-experimental-rocketry.md){: .pure-button .pure-button-primary }

## <i class="fa icon-swoords-crossed"></i> Boss Fight **Aerospace Engineering Degree**

<div class="cssProgress">
    <div class="progress3">
        <div class="cssProgress-bar cssProgress-danger" data-percent="92" style="width: 92%; transition:none;">
            <span class="cssProgress-label">101/110</span>
        </div>
    </div>
</div>

### <span style="color:rgb(0,68,119);"><i class="fa fa-2x icon-round-medal"></i> M.Sc. Aeronautics Engineering (Space Propulsion)</span>
### <span style="color:rgb(51,187,187);">+ <i class="fa fa-magic"></i> Numerical Modeling</span><br /><span style="color:rgb(255,187,102);"><i class="fa fa-user-plus"></i> Resilience (+5)</span>, <span style="color:rgb(255,187,102);"><i class="fa fa-user-plus"></i> Intelligence (+3)</span>, <span style="color:rgb(255,68,85)">Enthusiasm (-1)</span>

An adventure without a boss to defeat with wit and ability would not be worth to be lived. My first tough struggle to overcome to become the hero I want was beating the **M.Sc. Aeronautics Engineering Degree Guardian <i class="fa fa-shield"></i>**. 5.5 years of training and exercise were needed to build a solid moveset and to improve my skills and to finally obtain the desired degree. The road to success was full of obstacles, or maybe just growing up makes difficult for people to keep being enthusiastic about what they do: something that at the beginning I was not expecting, I thought university was the so-called *Temple of Knowledge* that was narrated in the ancient tales. That was not fully the case, at least for me. I encountered somehow ruins of a bygone era, people were just thinking to themselves renouncing to enlightenment and enthusiasm for the sake of personal, earthly, success.
At the end of this first milestone I decided that my role in this world would have been to pursue my inspiration research; and while looking for it trying to inspire others.

[More ...]({filename}/pages/polimi.md){: .pure-button .pure-button-primary }

>Like travelers along a Möbius strip, we all share the same surface even when we think we are at opposite side ([@alanbld](https://twitter.com/alanbld) Twitter)

## Learning an arcane language
### <span style="color:rgb(51,187,187);">+ <i class="fa fa-magic"></i> Fluid Dynamics</span>, <span style="color:rgb(51,187,187);">+ <i class="fa fa-magic"></i> CFD</span><br /><span style="color:rgb(255,187,102);"><i class="fa fa-user-plus"></i> Project Management (+2)</span>, <span style="color:rgb(255,187,102);"><i class="fa fa-user-plus"></i> Team Working (+2)</span>, <span style="color:rgb(255,187,102)"><i class="fa fa-user-plus"></i>Enthusiasm (+3)</span>

$$ \nabla \times \mathbf{u} = \mathbf{0} $$
During my travels and my training as a *science druid*, I often encountered strange symbols engraved on runes, signs of an ancient knowledge that I was told to be the key to lots of secrets that drive our world. 
I was fascinated by the complicated matter and I wanted to know more, but I needed to reach the place where someone could teach me how to interpret the language. **von Karman Institute for Fluid Dynamics**, an ancient and magic refuge in the middle of the [*Sonienwald*](http://www.foret-de-soignes.be), the perfect place to be for a druid. Here I worked on new formulas for potions, so-called **slush flows**, two-phase solid-liquid fluids meant to be used as fuel and/or oxidizer of the new generation space access systems.


[More ...]({filename}/pages/vki.md){: .pure-button .pure-button-primary }

>If you want to go fast, go alone. If you want to go far, go together (African Proverb)

## The tree of life
### <span style="color:rgb(51,187,187);">+ <i class="fa fa-magic"></i> Python Language</span>, <span style="color:rgb(51,187,187);">+ <i class="fa fa-magic"></i> Linux sysadmin</span><br /><span style="color:rgb(255,187,102);"><i class="fa fa-user-plus"></i> Project Management (+2)</span>, <span style="color:rgb(255,187,102);"><i class="fa fa-user-plus"></i> Programming (+2)</span>, <span style="color:rgb(255,187,102)"><i class="fa fa-user-plus"></i>Enthusiasm (+5)</span>

While you live the acts of your life opera, you can't gain enough awareness to perceive that, even if you think you're running alone, there are instead lots of people on your side that share your vision and dreams. In my early adventure in the [Skyward Experimental Rocketry](http://skywarder.eu) Guild, I met a group o visionary fellows that decided to invest their knowledge to build a network of ground stations all over the world, [Leaf Line](https://leaf.space/leaf-line) <i class="fa fa-leaf"></i>, to make access to space easier for everyone. I joined the startup as a part-time internship, with the task of developing **pyggdrasill**: some sort of magic that runs on those machines called computers and decides in the best way possible how to schedule satellites on ground stations. Amazing.


[More ...]({filename}/pages/leaf-space.md){: .pure-button .pure-button-primary }

*[RPGs]: Role-Playing Games
*[NPC]: Non-playing Character
*[HTPB]: Hydroxil-terminated PolyButadiene

