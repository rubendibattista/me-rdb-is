Title: Skyward Experimental Rocketry
Slug: skyward

[Skyward Experimental Rocketry](https://www.skywarder.eu) is a student association whose aim is to design and launch sounding rockets. I joined the association since its foundation, I occupied different positions within the team (Webmaster, Head of the Department of Computational Rocket Dynamics, President) and I've been member for 4 years. In this page you will find the main outcomes I achieved during my experience there.

## International Astronautical Congress 2016 (IAC-16,E1,5,5,x34251)
### Importance and Challenges of Hands-on experience in Astronautical Education
This paper discusses the role of hands-on-activities in tertiary education with particular focus on the space sector in Europe. Such activities include European educational programmes managed and supported by several national space agencies and the European Space Agency (ESA), such as REXUS/BEXUS (Rocket Experiments for University Students / Balloon Experiments for University Students), medium-sized national programmes like the STERN (Studentische Experimental-Raketen, Germany) or PERSEUS (Projet Etudiant de Recherche Spatiale Européen Universitaire et Scientifique) as well as smaller projects run by student organisations themselves.
First of all, the quantitative value of practical education will be presented to display the percentage of students who participate in hands-on-activities. Moreover, the impact of such initiatives will be detailed, followed by a discussion of the roles of different stakeholders like students, universities and industry.  Another major point of this contribution is the depiction of administrative and executional challenges that come along with the conduction of hand-on-activities with respect to ordinary theoretical education. Concluding, recommendations to improve this relation and thus the education of young space professionals will be given.
This contribution bases on the main tasks identified by the working group that addressed to “How can universities contribute to the acquisition of hands-on experience?” during the first European Student Workshop held in Padova in December 2015 in conjunction with the 1st Symposium on Space Educational Activities.

[Paper](https://iafastro.directory/iac/archive/browse/IAC-16/E1/5/34251/){: .pure-button .pure-button-primary }

## European Aerospace Students Meeting 2015 (EASM 2015)
### A 2-days symposium dedicated to student development of rocketry projects

<div class="yt-embed">
<iframe width="100%" height="480" src="https://www.youtube.com/embed/RmYpR3RuZ9M" frameborder="0" allowfullscreen></iframe>
</div>

Main creator and speaker of the first <abbr title="European Aerospace Students Meeting">EASM</abbr> dedicated to the presentation of rocketry projects developed by students all around Europe and World. A 2-days symposium mainly dedicated to students that are enrolled in pushing hands-on projects and aimed to present the student activities in the aeronautics and space field. On the side presentations and testimonies from the world of innovative start-ups, international and national aviation enterprises.

[Website](https://skywarder.eu/blog/en/easm-2015){: .pure-button .pure-button-primary }

## 6 d.o.f. Rocket Flight Simulator 
![r1x](https://www.skywarder.eu/blog/wp-content/uploads/2014/03/render2.46.jpg){: width=100%}

Skyward Experimental Rocketry is the first Italian student association devoted to the design, development and launch of sounding rockets. The first rocket developed by the association is [**R1X**](http://skywarder.eu/blog/rocksanne-i-x), a 2m-tall rocket powered by a $KNO_3 + \text{sugar} + HTPB$ solid rocket motor with a peak thrust of $780\text{N}$. During the launch campaign of R1X the task of my team was to develop simulation tools to predict apogee height and landing area of the rocket. A 6 d.o.f. simulator has been developed and validated against flight data taken during the three launches of the rocket.

The simulator employs quaternions to manage rotations between reference frames and the `ode45` provided by MATLAB for the time-integration of the differential equations system. 

[Download Report](https://drive.google.com/file/d/0B7DR4Jvb-dBRVF93WkJEeHRGM2c/view?usp=sharing){: .pure-button .pure-button-primary}
[Github](https://github.com/skyward-er/skyward-matlab-rocket-simulator){: .pure-button .pure-button-primary }
