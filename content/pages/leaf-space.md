Title: Leaf Space work
slug: leaf-space

[Leaf Space](https://leaf.space) is a space startup that was born in 2014 with the aim of simplifying access to space. The current ongoing project is about the design and implementation of a ground-stations newtork, called [Leaf Line](https://leaf.space/leaf-line), that is completely dedicated to serve microsatellites operators. I worked there in the frame of two internships and my main task was to develop a scheduling algorithm to solve the *Satellite Range Scheduling* problem that is currently running on staging environment to be tested against real job flow.

## Scheduling Software
### pyggdrasill

**pyggdrasill** is the software library developed internally to [Leaf Space](https://leaf.space) that is meant to take as input the parameters of all the satellites operating on the [Leaf Line](https://leaf.space/leaf-line) network and the currently online ground-stations and compute the optimal schedule for the actual situation.
Currently the code works performing a Monte Carlo optimization of the solution required and is currently operating on the testing network. The code is written in Python.

## International Astronautical Congress 2016 (IAC-16,B4,3,8,x34546)
###Solution for a ground station network providing a high bandwidth and high accessibility data link for nano- and microsatellite
During the last years, nano- and microsatellite technologies have encountered a fast development materialized in projection of thousands of satellites to be launched in Low-Earth Orbit (LEO) in the near future. However, the ground segment has not encountered a similar development leaving a gap in the possibilities of communicating and operating an increased number of satellites. The advent of the huge constellations (e.g. OneWeb, PlanetLabs, Spire, Satellogic etc.) has made clear the necessity of downloading massive amount of data from space simplifying the management of multi-satellite mission. In addition, it will be needed a real-time access to downloaded data for a number of scenarios (e.g. refugees monitoring, disaster management, environment surveillance, etc.) able to fostering also downstream applications. 

In this paper a solution to these issues has been identified by envisioning a global network of 20 autonomous ground stations operating on different frequency bands (VHF, UHF, S-Band, X-Band) with different protocols and modulations, connected on the Internet and operated remotely. The solution proposed has been carried out by Leaf Space srl, an Italian startup based in Milan in collaboration with Université de Picardie Jules Verne, based on innovative ground station scheduling algorithms and tasks automation on commercial hardware, in order to provide a quality, reliable and money-saving alternative to current state-of-the-art.

[Paper](https://iafastro.directory/iac/archive/browse/IAC-16/B4/3/34546/){: .pure-button .pure-button-primary}
