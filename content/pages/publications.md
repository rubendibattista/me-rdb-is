Title: Publications
slug: publications

## International Astronautical Congress 2016 (IAC-16,B4,3,8,x34546)
###Solution for a ground station network providing a high bandwidth and high accessibility data link for nano- and microsatellite
During the last years, nano- and microsatellite technologies have encountered a fast development materialized in projection of thousands of satellites to be launched in Low-Earth Orbit (LEO) in the near future. However, the ground segment has not encountered a similar development leaving a gap in the possibilities of communicating and operating an increased number of satellites. The advent of the huge constellations (e.g. OneWeb, PlanetLabs, Spire, Satellogic etc.) has made clear the necessity of downloading massive amount of data from space simplifying the management of multi-satellite mission. In addition, it will be needed a real-time access to downloaded data for a number of scenarios (e.g. refugees monitoring, disaster management, environment surveillance, etc.) able to fostering also downstream applications. 

In this paper a solution to these issues has been identified by envisioning a global network of 20 autonomous ground stations operating on different frequency bands (VHF, UHF, S-Band, X-Band) with different protocols and modulations, connected on the Internet and operated remotely. The solution proposed has been carried out by Leaf Space srl, an Italian startup based in Milan in collaboration with Université de Picardie Jules Verne, based on innovative ground station scheduling algorithms and tasks automation on commercial hardware, in order to provide a quality, reliable and money-saving alternative to current state-of-the-art.

[Paper](https://drive.google.com/open?id=0B7DR4Jvb-dBRc3FNSEp6RERaNDg){: .pure-button .pure-button-primary}


## International Astronautical Congress 2016 (IAC-16,E1,5,5,x34251)
### Importance and Challenges of Hands-on experience in Astronautical Education
This paper discusses the role of hands-on-activities in tertiary education with particular focus on the space sector in Europe. Such activities include European educational programmes managed and supported by several national space agencies and the European Space Agency (ESA), such as REXUS/BEXUS (Rocket Experiments for University Students / Balloon Experiments for University Students), medium-sized national programmes like the STERN (Studentische Experimental-Raketen, Germany) or PERSEUS (Projet Etudiant de Recherche Spatiale Européen Universitaire et Scientifique) as well as smaller projects run by student organisations themselves.
First of all, the quantitative value of practical education will be presented to display the percentage of students who participate in hands-on-activities. Moreover, the impact of such initiatives will be detailed, followed by a discussion of the roles of different stakeholders like students, universities and industry.  Another major point of this contribution is the depiction of administrative and executional challenges that come along with the conduction of hand-on-activities with respect to ordinary theoretical education. Concluding, recommendations to improve this relation and thus the education of young space professionals will be given.
This contribution bases on the main tasks identified by the working group that addressed to “How can universities contribute to the acquisition of hands-on experience?” during the first European Student Workshop held in Padova in December 2015 in conjunction with the 1st Symposium on Space Educational Activities.


[Paper](https://drive.google.com/open?id=0B7DR4Jvb-dBRZTdzTlBrTGlLX1k){: .pure-button .pure-button-primary }
