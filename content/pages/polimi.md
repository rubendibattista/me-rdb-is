Title: Politecnico di Milano
Slug: polimi

I graduated at [Politecnico di Milano](http://www.aero.polimi.it) in M.Sc. Aeronautics Engineering with a minor in Space Propulsion. In this page I selected some material that could be interesting to be shared. Feel free to download it.

## Master Thesis 
### *Numerical Simulation of Two-Phase Solid-Liquid Slush Flows for Propulsion Systems*
Slushes are two-phase solid-liquid single-species cryogenic fluids that exhibit an increased density and a greater heat capacity with respect to normal boiling point fluids. This promising features are of big interest for applications that exploit the slush as a thermal fluid, like super magnets refrigeration or air conditioning, and for aerospace systems that use slush fluids as fuel or oxidizer. Several pro- grams in the frame of the research on Slush Hydrogen (SLH2) as a new-generation fuel for aerospace propulsion system have been started in the past and still continue to be performed in the present (National Aeronautics and Space Administration (NASA)’s National Space Plane (NASP), European Space Agency (ESA)’s Future European Space Transportation Investigations Programme (FESTIP) and Japan Aerospace eXploration Agency (JAXA) program for researh on SLH2 are the most famous examples).
In this work a numerical simulation based on a finite-volumes discretization using the software library OpenFOAM is carried on solid-liquid multiphase flows (slurry) and slush flows inside a typical pipe geometry, very common in propulsion pipelines. A benchmark with previous experiments and simulations is also performed to assess the degree of accuracy of the code in predicting pressure drops and solid phase fraction dispersion. The effects of particle size, inlet velocity and concentration is also investigated.

[Download Full Text](https://drive.google.com/file/d/0B7DR4Jvb-dBRT2lkWTVJQ0FjX1U/view?usp=sharing){: .pure-button .pure-button-primary }

## Nanomaterials Course Project
### Image processing automated procedure for the time-resolved analysis of hybrid solid fuel formulations using a RANSAC algorithm
In energetic applications Hybrid Rocket Engines (HREs) have showed promising performances in therm of pure Is and in term of economical impact and setup easiness in respect to Liquid Rocket Engines (LREs) and Solid Rocket Motors (SRMs). Moreover historically the burning rate of the formulations, especially for SRMs has been investigated through Thickness over time (TOT) techniques leading to average estimations that could be inaccurate, especially for HREs. In this article an automated image processing time resolved analysis is presented and its feasibility is discussed.


[Download Full Text](https://drive.google.com/open?id=0B2I_rXqzHyG8bEo0dldIZ2VnS28){: .pure-button .pure-button-primary }
[Github Repository](https://github.com/rubendibattista/python-ransac-library){: .pure-button .pure-button-primary}

*[Is]: Specific Impulse
