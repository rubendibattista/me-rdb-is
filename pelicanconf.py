#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Ruben Di Battista'
SITENAME = 'RdB Personal Page'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True


# Not mainly a blog configuration
ARTICLE_PATHS = ['blog']
ARTICLE_URL = 'blog/{slug}'
ARTICLE_SAVE_AS = 'blog/{slug}.html'
PAGE_PATHS = ['pages']
PAGE_URL = '{slug}'
PAGE_SAVE_AS = '{slug}.html'
# INDEX_SAVE_AS = 'blog-index.html'
CATEGORY_SAVE_AS = 'blog/category/{slug}.html'
CATEGORY_URL = 'blog/category/{slug}'
TAG_SAVE_AS = 'blog/tags/{slug}.html'
TAG_URL = 'blog/tags/{slug}'
STATIC_PATHS = ['img']

# Theme-specific settings
THEME = 'rdb-pure'
SIDEBAR_IMG_URL = 'img/side-image.jpg'
SHOW_AUTHOR_INFO = False
PERSONAL_MENUITEMS = (('My Publications', SITEURL+'/publications'),
                      ('VKI Work', SITEURL+'/vki'),
                      ('PoliMi Work', SITEURL+'/polimi'),
                      ('Skyward Work', SITEURL+'skyward')
                      )

# Plugins
PLUGIN_PATHS = ['custom-plugins', 'pelican-plugins']
PLUGINS = ['render_math']

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'highlight'},
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
    },
    'output_format': 'html5',
}
