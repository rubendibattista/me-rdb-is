This website is generated using [Pelican](https://getpelican.com). 

# Copyright 
## Icon sets
- [Fontawesome](http://fontawesome.io)
- Scientific Study & Playing Videogames [by Freepik](http://flaticon.com)

## Other Software
- [Pelican Plugins](https://github.com/getpelican/pelican-plugins/blob/master/LICENSE)
- [PureCSS](https://github.com/yahoo/pure/blob/master/LICENSE.md)
- [Parallax](https://github.com/wagerfield/parallax/blob/master/LICENSE)


# Licensing
- [AGPL](https://gitlab.com/rubendibattista/me-rdb-is/blob/develop/LICENSE.md)
